import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {selectUser} from '../Actions'

class UserList extends Component {
    createList() {
        return this.props.users.map((user)=> {
            return <li 
                        key={user.id} 
                        onClick={ () => this.props.selectUser(user) }
                    >
                        {user.first} {user.last}
                    </li>
        });
    }

    render() {
        return (
            <ul>
                {this.createList()}
            </ul>
        );
    }
}

// Get app state and pass it as props to UserList
function mapStateToProps(state) {
    return {
        users: state.users
    }
}

// Get action function and pass it as props to UserList
function matchDispatchToProps(dispatch) {
    return bindActionCreators({selectUser: selectUser}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserList);