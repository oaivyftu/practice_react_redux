import React, {Component} from 'react';
import UserList from '../Containers/UserList';
import UserDetail from '../Containers/UserDetail';

export default class App extends Component {
    render() {
        return (
            <div>
                <UserList />
                <hr />
                <UserDetail />
            </div>
        );
    }
}