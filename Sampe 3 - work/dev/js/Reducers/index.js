import allUsers from './reducer-all-users'
import activeUser from './reducer-active-user'
import {combineReducers} from 'redux'

const allReducers = combineReducers({
    users       : allUsers,
    activeUser  : activeUser
});

export default allReducers;