import {combineReducers} from 'redux';
import UserReducer from './reducer-users';
import UserActive from './reducer-user-active';

const allReducers = combineReducers({
    users       : UserReducer,
    activeUser  : UserActive
});

export default allReducers;