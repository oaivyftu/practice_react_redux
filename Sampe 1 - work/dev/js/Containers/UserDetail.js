import React, {Component} from 'react';
import {connect} from 'react-redux';

class UserDetail extends Component {
    render() {
        if(!this.props.activeUser) {
            return <p>Select User...</p>;
        }
        return(
            <div>
                <img src={this.props.activeUser.thumbnail} alt=""/>
                <h4>Name: {this.props.activeUser.first} {this.props.activeUser.first}</h4>
                <p>Description: {this.props.activeUser.description}</p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        activeUser: state.activeUser
    }
}

export default connect(mapStateToProps)(UserDetail);