import {combineReducers} from 'redux'
import UsersReducer from './reducer-users';
import ActiveUser from './reducer-user-active';

const allReducers = combineReducers({
    users: UsersReducer,
    activeUser: ActiveUser
});

export default allReducers;