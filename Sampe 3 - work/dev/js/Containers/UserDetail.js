import React, {Component} from 'react'
import {connect} from 'react-redux'

/*
 * We need "if(!this.props.user)" because we set state to null by default
 * */
class UserDetail extends Component {
    render() {
        if(!this.props.user) {
            return <p>Select a user..</p>
        }
        return (
            <div>
                <h2>{this.props.user.first} {this.props.user.last}</h2>
                <img src={this.props.user.thumbnail} alt=""/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    };
}

export default connect(mapStateToProps)(UserDetail);