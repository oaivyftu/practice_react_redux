import React from 'react';
import UserList from '../Containers/UserList';
import UserDetail from '../Containers/UserDetail';

export default class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Welcome to this app</h1>
                <UserList />
                <hr />
                <UserDetail />
            </div>
        )
    }
}